const LodashModuleReplacementPlugin = require('lodash-webpack-plugin')
const path = require('path');
const webpackDir = path.resolve(__dirname);
const distDir = path.resolve(webpackDir, 'dist');

module.exports = {
  entry: './src/liveblogStream.js',
  output: {
    path: distDir,
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.json$/,
        exclude: /node_modules/,
        loader: 'json'
      }
    ]
  },
  devtool: 'source-map',
  plugins: [
    new LodashModuleReplacementPlugin
  ]
}
