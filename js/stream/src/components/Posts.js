import React, { Component } from 'react'
import ScrollPosition from '../helper/ScrollPosition'
import Notification from './Notification'
import Post from './Post'
import axios from 'axios';

export default class Posts extends Component {

  constructor() {
    super()
    this.state = {
      posts: [],
      newPosts: [],
      loadNextPosts: []
    }
    this.isloading = false
    this.hasReachedEnd = false
    this.postNodes = {}
    this.post_data = ""
    this.newLoadNextPosts = {}
    this.loadMore = this.loadMore.bind(this)
    this.checkForNextitem = this.checkForNextitem.bind(this)
  }

  componentWillMount() {
    this.setState({ isLoading: true });
    axios.get(this.props.getURL)
      .then(posts => {
        if (Array.isArray(posts.data.content)) {
          this.setState({
            posts: posts.data.content
          })
          this.props.onPostLoad(posts, document.body)
          this.checkForNextitem()
        }
      })
      .catch((error) => console.log('error', error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  }

  loadMore() {
    if(this.state.loadNextPosts && Array.isArray(this.state.loadNextPosts) && (this.state.loadNextPosts.length != 0)) {
      this.newLoadNextPosts = this.state.loadNextPosts;
      this.state.loadNextPosts = [];
      this.setState({
        posts: [
          ...this.state.posts,
          ...this.newLoadNextPosts[0].data.content,
        ]
      }, () => this.checkForNextitem())
      this.props.onPostLoad(this.newLoadNextPosts[0], document.body)
    }
  }

  checkForNextitem() {
    let posts = this.state.posts;
    let lastPost = posts[posts.length - 1]
    let url = this.props.getNextURL.replace('%s', lastPost.created)
    this.setState({ isLoading: true });
    axios.get(url)
      .then(lazyPosts => {
        if (lazyPosts && Array.isArray(lazyPosts.data.content)) {
          if (lazyPosts.data.content.length != 0) {
            this.setState({
              loadNextPosts: [
                lazyPosts
              ]
            })
          }
        }
      })
      .catch((error) => console.log('error', error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  }

  addPost(post) {
    const rect = this.postsWrapper.getBoundingClientRect()

    if(post.final) {
      this.post_data += post.content;
      post.content = this.post_data;
      this.post_data = '';
      if (rect.top < 0 || this.state.newPosts.length > 0) {
        this.setState({
          newPosts: [
            post,
            ...this.state.newPosts
          ]
        })
      } else {
        this._loadPosts([post])
      }
    }
    else {
      this.post_data += post.content;
    }

  }

  editPost(editedPost) {
    let found = false

    if(editedPost.final) {
      this.post_data += editedPost.content;
      editedPost.content = this.post_data;
      this.post_data = '';
      const posts = this.state.posts.map((post) => {
        if (post.id == editedPost.id) {
          found = true
          return editedPost
        } else {
          return post
        }
      })
      if (found) {
        const scrollPosition = new ScrollPosition(document.body, this.postNodes[editedPost.id])
        scrollPosition.prepareFor('up')

        this.setState({
          posts: posts
        })

        this.props.onPostLoad(editedPost, document.body)
        scrollPosition.restore()
      }
    }
    else {
      this.post_data += editedPost.content;
    }
  }

  _loadNewPosts() {
    const rect = this.postsWrapper.getBoundingClientRect()
    const bodyRect = document.body.getBoundingClientRect()
    jQuery('html, body').animate({
      scrollTop: rect.top - bodyRect.top - 90
    }, () => {
      this._loadPosts(this.state.newPosts)
    })
  }

  _loadPosts(posts) {
    this.setState({
      posts: [
        ...posts,
        ...this.state.posts
      ],
      newPosts: []
    })

    for (let i=0; i<posts.length; i++) {
      const newPost = posts[i]
      this.props.onPostLoad(newPost, document.body)
    }
  }

  render() {
    let button = "";
    if(this.state.loadNextPosts && Array.isArray(this.state.loadNextPosts) && (this.state.loadNextPosts.length != 0)) {
      button = <div className="liveblog-show-more"><button onClick={this.loadMore} className="btn-grad">{this.isLoading ? 'Loading...' : 'Show more'}</button></div>;
    }
    return (
      <div className="liveblog-posts-wrapper" ref={(wrapper) => {this.postsWrapper = wrapper}}>
  <Notification newPosts={this.state.newPosts} loadNewPosts={this._loadNewPosts.bind(this)} />
    { this.state.posts.map((post) => (
      <div className="liveblog-post" key={post.id} ref={(node) => { this.postNodes[post.id] = node }}>
    <Post content={post.content} />
    </div>
    ))}
    {button}
  </div>
  )
  }

}
